FROM python:2.7-alpine

ARG APP_HOME="/app" 

WORKDIR "$APP_HOME" 

ADD requirements.txt ${APP_HOME}/requirements.txt

RUN pip install -r requirements.txt

COPY . ${APP_HOME}/

ARG user=bender
ARG group=robots
ARG uid=11111
ARG gid=22222

RUN addgroup -g ${gid} -S ${group} && \
    adduser  -u ${uid} -S ${user} -G ${group}

RUN chown -R ${user} "${APP_HOME}" 

CMD ["python", "app.py"]


