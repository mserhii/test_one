import json
from collections import OrderedDict

def parse_f():
    with open('key_context_location.json') as f:
        data = json.load(f, object_pairs_hook=OrderedDict)
#        print(data)
    return (data)

if __name__ == "__main__":
    parse_f()

files_list = parse_f()
#print(files_list.values())

#with open("context/last_names.txt") as f:
#    last = f.readlines()

#last = [x.strip() for x in last]
#print(last)

result = {}
v = list(files_list.values())
last_name = []
values = []

for x in range(4):
    with open(v[x]) as f:
#        print(v[x])
        if x == 0:
          last_name = f.readlines()
          last = [x.strip() for x in last_name]
          #print(last)
        else:
          values_f = f.readlines()
          values_f = [x.strip() for x in values_f]
          values.append(values_f)
          #print(values)

res = dict(zip(last_name, values))

for last, first, dest, src  in zip(last, values[0],values[1],values[2]):
  result[last] = [first, dest, src]

print(json.dumps(result))
